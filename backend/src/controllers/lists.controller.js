import * as ListsService from '../services/lists.service.js'; 

/* LISTS CONTROLLERS */

export const getLists = async (req, res) => {
    try {
        const lists = await ListsService.getLists();
        return res.status(200).json({data: lists});
    } catch (error) {
        return res.status(400).json({message: "Error !"});
    }
}

export const getList = async (req, res) => {
    try {
        const list = await ListsService.getList(req.params.id);
        return res.status(200).json(list);
    } catch (error) {
        return res.status(400).json({message: "Error !"});
    }
}

export const createList = async (req, res) => {
    try {
        const list = req.body;
        const lists = await ListsService.createList(list);
        return res.status(200).json({ status: 200, data: lists, message: "Succesfully Lists created" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

export const updateList = async (req, res) => {
    try {
        const list = req.body;
        const listUpdated = await ListsService.updateList(list);
        return res.status(200).json({ status: 200, data: listUpdated, message: `List with id ${list._id} succesfully updated` });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message});
    }
}


export const deleteList = async (req, res) => {
    try {
        const { listId } = req.params;
        const lists = await ListsService.deleteList(listId);
        return res.status(200).json({ status: 200, data: lists, message: `List with id ${listId} succesfully deleted` });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

/* TASKS CONTROLLERS */

export const getListTasks = async (req, res) => {
    try {
        const { listId } = req.params;
        const tasks = await ListsService.getListTasks(listId);
        return res.status(200).json({data: tasks});
    } catch (error) {
        return res.status(400).json({message: "Error !"});
    }
}

export const getListTask = async (req, res) => {
    try {
        const { listId, taskId } = req.params;
        const task = await ListsService.getListTask(listId, taskId);
        return res.status(200).json(task);
    } catch (error) {
        return res.status(400).json({message: "Error !"});
    }
}

export const createListTask = async (req, res) => {
    try {
        const { listId } = req.params;
        const task = req.body;
        const tasks = await ListsService.createListTask(listId, task);
        return res.status(200).json({ status: 200, data: tasks, message: "Succesfully Lists created" });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

export const updateListTask = async (req, res) => {
    try {
        const { listId } = req.params;
        const task = req.body;
        const taskUpdated = await ListsService.updateListTask(listId, task);
        return res.status(200).json({ status: 200, data: taskUpdated, message: `List with id ${task._id} succesfully updated` });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message});
    }
}

export const deleteListTask = async (req, res) => {
    try {
        const { listId, taskId } = req.params;
        const tasks = await ListsService.deleteListTask(listId, taskId);
        return res.status(200).json({ status: 200, data: tasks, message: `Task with id ${taskId} succesfully deleted` });
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}