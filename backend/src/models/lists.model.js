import mongoose from 'mongoose';

const { Types } = mongoose; 

const ListSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  tasks: [
        {
            /* _id: {
              type: Types.ObjectId,
              required: true
            }, */
            label: {
                type: String,
                required: true
            },
            done: {
                type: Boolean,
                default: false
            },
        }
    ],
  },


{ versionKey: false },);

const List = mongoose.model("Lists", ListSchema);

export default List;