import List from "../models/lists.model.js"

/* LISTS SERVICES */

export const getLists = async () => {
    try {
        return await List.find();
    } catch (e) {
        throw Error('Error.');
    }
}

export const getList = async (id) => {
    try {
        return await List.findById(id);
    } catch (error) {
        throw Error('Error.');
    }
}

export const createList = async (list) => {
    try {
        const listToCreate = new List(list);
        await listToCreate.save();
        return await getLists();
    } catch (e) {
        throw Error('Error.');
    }
}

export const updateList = async (list) => {
    try {
        await List.findByIdAndUpdate(list._id, list);
        return list;
    } catch (e) {
        throw Error('Error.');
    }
}

export const deleteList = async (listId) => {
    try {
        await List.deleteOne({ _id: listId });
    } catch (e) {
        throw Error('Error.');
    }
}

/* TASKS SERVICES */

export const getListTasks = async (id) => {
    try {
        const list = await getList(id);
        return list.tasks;
    } catch (e) {
        throw Error('Error.');
    }
}

export const getListTask = async (listId, taskId) => {
    try {
        const tasks = await getListTasks(listId);
        return tasks.find((t) => { return t.id === taskId});
    } catch (error) {
        throw Error('Error.');
    }
}

export const createListTask = async (listId, task) => {
    try {
        await List.updateOne( 
            { _id: listId },
            { $push: { tasks : task } }
        )
        return await getListTasks(listId);
    } catch (e) {
        throw Error('Error.');
    }
}

export const updateListTask = async (listId, task) => {
    try {
        await List.updateOne(
            { "_id": listId,  "tasks._id": task._id },
            { $set: { "tasks.$.label" : task.label, "tasks.$.done" : task.done } }
        )
        return await getListTasks(listId);
    } catch (e) {
        throw Error('Error.');
    }
}

export const deleteListTask = async (listId, taskId) => {
    try {
        await List.updateOne( 
            { _id: listId },
            { $pull: { tasks : {_id: taskId} } }
        )
        return await getListTasks(listId);
    } catch (e) {
        throw Error('Error.');
    }
}