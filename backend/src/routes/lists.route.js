import express from "express";
import { getLists, getList, createList, updateList, deleteList, getListTasks, 
    getListTask, createListTask, updateListTask, deleteListTask } from "../controllers/lists.controller.js";

const router = express.Router();

/* LISTS ROUTERS */

router.get("/", getLists);

router.get("/:id", getList);

router.post('/', createList);

router.put('/', updateList);

router.delete('/:listId', deleteList);

/* TASKS ROUTERS */

router.get("/:listId/tasks", getListTasks);

router.get("/:listId/tasks/:taskId", getListTask);

router.post('/:listId/tasks', createListTask);

router.put('/:listId/tasks', updateListTask);

router.delete('/:listId/tasks/:taskId', deleteListTask);

export default router;