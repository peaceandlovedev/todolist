import bodyParser from 'body-parser';
import express from 'express';
import listsRoutes from "./src/routes/lists.route.js"
import mongoose from 'mongoose';
import cors from 'cors';

const hostname = '127.0.0.1';
const port = 5000;

// Connexion à la base de données
mongoose.connect('mongodb+srv://testapi:Anisoft17@cluster0.kijdy.mongodb.net/todoList?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);

const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
  console.log("Connected successfully");
});

const app = express();

app.use(cors());

app.use(bodyParser.json());

app.use("/lists", listsRoutes);

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});